package com.example.sputyfive;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button Play;
    private MediaPlayer MP;
    private ImageView ImV;
    private TextView txtName;

    private MediaPlayer VectorPlayer [] = new MediaPlayer[3];

    private ArrayList<String> Nombres = new ArrayList<>();
    private ArrayList<String> Art = new ArrayList<>();

    private ArrayList<Integer> Imagenes = new ArrayList<Integer>();

    private int posicion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Play = (Button) findViewById(R.id.Play);
        ImV = (ImageView) findViewById(R.id.ImV);
        txtName = (TextView) findViewById(R.id.txtNombre);

        VectorPlayer [0] = MediaPlayer.create(this, R.raw.race);
        VectorPlayer [1] = MediaPlayer.create(this, R.raw.sound);
        VectorPlayer [2] = MediaPlayer.create(this, R.raw.tea);

        Nombres.add("race");
        Nombres.add("sound");
        Nombres.add("tea");
        Art.add("ATE");
        Art.add("cask feat. lil.will");
        Art.add("china");

        Imagenes.add(R.drawable.portada1);
        Imagenes.add(R.drawable.portada2);
        Imagenes.add(R.drawable.portada3);

    }

    public void onPlay(View v){
        if (VectorPlayer[posicion].isPlaying()){
            VectorPlayer[posicion].pause();
            txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            Play.setBackgroundResource(R.drawable.reproducir);
            Toast.makeText(this, "Pausa", Toast.LENGTH_SHORT).show();
        }else{
            VectorPlayer[posicion].start();
            Play.setBackgroundResource(R.drawable.pausa);
            txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            ImV.setImageResource(Imagenes.get(posicion));
            Toast.makeText(this, "Play", Toast.LENGTH_SHORT).show();
        }
    }

    public void Next(View v){
        if (posicion<VectorPlayer.length-1){
            if(VectorPlayer[posicion].isPlaying()){
                VectorPlayer[posicion].stop();
                posicion++;
                VectorPlayer[posicion].start();
                ImV.setImageResource(Imagenes.get(posicion));
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());

            }else {
                posicion++;
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
                ImV.setImageResource(Imagenes.get(posicion));
            }
        }else{
            if(VectorPlayer[posicion].isPlaying()) {
                VectorPlayer[posicion].stop();
                posicion = 0;
                VectorPlayer[0] = MediaPlayer.create(this, R.raw.race);
                VectorPlayer[1] = MediaPlayer.create(this, R.raw.sound);
                VectorPlayer[2] = MediaPlayer.create(this, R.raw.tea);
                ImV.setImageResource(Imagenes.get(posicion));
                VectorPlayer[posicion].start();
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            }else{
                posicion = 0;
                VectorPlayer[0] = MediaPlayer.create(this, R.raw.race);
                VectorPlayer[1] = MediaPlayer.create(this, R.raw.sound);
                VectorPlayer[2] = MediaPlayer.create(this, R.raw.tea);
                ImV.setImageResource(Imagenes.get(posicion));
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            }
        }
    }

    public  void Back(View v){
        if(posicion>0){
            if(VectorPlayer[posicion].isPlaying()){
                VectorPlayer[posicion].stop();
                VectorPlayer [0] = MediaPlayer.create(this, R.raw.race);
                VectorPlayer [1] = MediaPlayer.create(this, R.raw.sound);
                VectorPlayer [2] = MediaPlayer.create(this, R.raw.tea);
                posicion--;

                VectorPlayer[posicion].start();
                ImV.setImageResource(Imagenes.get(posicion));
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            }else{
                posicion--;
                ImV.setImageResource(Imagenes.get(posicion));
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            }

        }else{
            if(VectorPlayer[posicion].isPlaying()) {
                VectorPlayer[posicion].stop();
                posicion = VectorPlayer.length - 1;
                VectorPlayer[0] = MediaPlayer.create(this, R.raw.race);
                VectorPlayer[1] = MediaPlayer.create(this, R.raw.sound);
                VectorPlayer[2] = MediaPlayer.create(this, R.raw.tea);
                ImV.setImageResource(Imagenes.get(posicion));
                VectorPlayer[posicion].start();
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            }else{
                posicion = VectorPlayer.length - 1;
                VectorPlayer[0] = MediaPlayer.create(this, R.raw.race);
                VectorPlayer[1] = MediaPlayer.create(this, R.raw.sound);
                VectorPlayer[2] = MediaPlayer.create(this, R.raw.tea);
                ImV.setImageResource(Imagenes.get(posicion));
                txtName.setText(Nombres.get(posicion).toString()+"\n"+Art.get(posicion).toString());
            }
        }
    }
}
